from multiprocessing import freeze_support

from overlay_core.overlay import Overlay
from overlay_core.tray import Tray
from overlay_core.assets import news_ico


if __name__ == '__main__':
    freeze_support()
    tray = Tray(
        icon_image_path=str(news_ico),
        icon_name='NewsOverlay'
    )
    overlay = Overlay(tray=tray)
    overlay.run()
