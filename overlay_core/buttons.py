import tkinter as tk
from typing import List

from .constants import (
    HOVER_BUTTON_DEFAULT_FONT,
    HOVER_BUTTON_ENTER_BG,
    HOVER_BUTTON_DEFAULT_FONT_SIZE,
    HOVER_BUTTON_DEFAULT_FG,
    HOVER_BUTTON_DEFAULT_BG,
    HOVER_BUTTON_ENTER_FONT_SIZE,
    BACKGROUND,
    FOREGROUND,
    FONT,
    EventFunction,
    EVENT_LEAVE,
    EVENT_ENTER,
    EVENT_LEFT_BUTTON,
    DIRECTION_LEFT,
    DIRECTION_RIGHT,
    DIRECTION_DOWN,
    DIRECTION_TOP,
    Direction,
    DIRECTION_RIGHT_ICON,
    DIRECTION_DOWN_ICON,
    DIRECTION_LEFT_ICON,
    DIRECTION_TOP_ICON,
    CLOSE_BUTTON_DEFAULT_ICON,
    CLOSE_BUTTON_DEFAULT_FG
)


class HoverButton(tk.Label):

    """
    Label, which changes this color, when mouse hover to it
    """

    def __init__(self, master: tk.Misc, **kwargs):
        self.string = tk.StringVar()

        self._font_size = HOVER_BUTTON_DEFAULT_FONT_SIZE
        self._default_font = (
            HOVER_BUTTON_DEFAULT_FONT[0],
            self._font_size,
            HOVER_BUTTON_DEFAULT_FONT[-1]
        )

        default_kwargs = {
            FONT: self._default_font,
            FOREGROUND: HOVER_BUTTON_DEFAULT_FG,
            BACKGROUND: HOVER_BUTTON_DEFAULT_BG
        }

        default_kwargs.update(kwargs)

        super(HoverButton, self).__init__(
            master=master,
            textvariable=self.string,
            **default_kwargs
        )

        self.onclick_functions: List[EventFunction] = []
        self.onleave_functions: List[EventFunction] = []
        self.onenter_functions: List[EventFunction] = []

        self.bind(EVENT_LEFT_BUTTON, HoverButton.functions_wrapper(self.onclick_functions))
        self.bind(EVENT_LEAVE, HoverButton.functions_wrapper(self.onleave_functions))
        self.bind(EVENT_ENTER, HoverButton.functions_wrapper(self.onenter_functions))

        @self.on_leave
        def change_to_default() -> None:
            self[BACKGROUND] = HOVER_BUTTON_DEFAULT_BG
            self.font_size = HOVER_BUTTON_DEFAULT_FONT_SIZE

        @self.on_enter
        def change_to_gray() -> None:
            self[BACKGROUND] = HOVER_BUTTON_ENTER_BG
            self.font_size = HOVER_BUTTON_ENTER_FONT_SIZE

    @property
    def font_size(self) -> str:
        return self._font_size

    @font_size.setter
    def font_size(self, value: str) -> None:
        self._font_size = value
        self[FONT] = self._default_font

    @staticmethod
    def functions_wrapper(function_collection: List[EventFunction]) -> EventFunction:
        def wrapper(event: tk.Event) -> None:
            for function in function_collection:
                try:
                    function(event)
                except TypeError:
                    function()

        return wrapper

    def on_click(self, function: EventFunction) -> None:
        self.onclick_functions.append(function)

    def on_enter(self, function: EventFunction) -> None:
        self.onenter_functions.append(function)

    def on_leave(self, function: EventFunction) -> None:
        self.onleave_functions.append(function)


class ExpandButton(HoverButton):

    def __init__(
            self,
            master: tk.Misc,
            direction: str = DIRECTION_LEFT,
            **kwargs
    ):
        super(ExpandButton, self).__init__(
            master=master, **kwargs
        )
        self.direction = direction
        self.string.set(self.direction_arrow)

        @self.on_click
        def change_arrow_direction() -> None:
            self.direction = self.opposite_direction
            self.string.set(self.direction_arrow)

    @property
    def opposite_direction(self) -> Direction:
        return {
            DIRECTION_TOP: DIRECTION_DOWN,
            DIRECTION_LEFT: DIRECTION_RIGHT,
            DIRECTION_DOWN: DIRECTION_TOP,
            DIRECTION_RIGHT: DIRECTION_LEFT
        }[self.direction]

    @property
    def direction_arrow(self) -> Direction:
        return {
            DIRECTION_TOP: DIRECTION_TOP_ICON,
            DIRECTION_LEFT: DIRECTION_LEFT_ICON,
            DIRECTION_RIGHT: DIRECTION_RIGHT_ICON,
            DIRECTION_DOWN: DIRECTION_DOWN_ICON
        }[self.direction]


class CloseButton(HoverButton):

    def __init__(
            self,
            master: tk.Misc,
            **kwargs
    ):
        kwargs[FOREGROUND] = CLOSE_BUTTON_DEFAULT_FG
        super(CloseButton, self).__init__(master=master, **kwargs)
        self.string.set(CLOSE_BUTTON_DEFAULT_ICON)