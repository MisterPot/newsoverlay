import tkinter as tk
from PIL import Image, ImageTk
import threading

from .constants import (
    BACKGROUND,
    SETTING_WINDOW_DEFAULT_BG
)


class WaitWindow(tk.Toplevel):

    animation_image: Image.Image = None
    rotation_angle: int = 10
    animation_wait_time = 100

    def __init__(
            self,
            master: tk.Misc,
            **kwargs
    ):
        default_kwargs = {BACKGROUND: SETTING_WINDOW_DEFAULT_BG}
        default_canvas_kwargs = default_kwargs.copy()
        try:
            default_canvas_kwargs.update(kwargs.pop('canvas'))
        except KeyError:
            pass
        default_kwargs.update(kwargs)
        super(WaitWindow, self).__init__(master=master, **default_kwargs)
        self.canvas = tk.Canvas(master=self, **default_canvas_kwargs)
        self.canvas.pack()
        self.state_image: ImageTk.PhotoImage = None
        self.stop_event = threading.Event()
        self.current_angle = 0
        self.timer_id: int = None
        self.current_animation_id: int = None

        width = self.winfo_reqwidth()
        height = self.winfo_reqheight()
        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()

        self.geometry(
            f"{width}x{height}"
            f"+{(screen_width - width) // 2}"
            f"+{(screen_height - height) // 2}"
        )

        self.overrideredirect(True)
        self.wm_attributes("-topmost", True)
        self.wm_attributes('-alpha', 0.6)
        self.lift()

    def start_animation(self) -> None:
        if self.current_animation_id is not None:
            return

        self.stop_event.clear()
        self.current_animation_id = self.master.after(10, self.animate)

    def animate(self) -> None:

        if self.state_image is None:
            self.state_image = ImageTk.PhotoImage(self.animation_image)
        else:
            rotated = self.animation_image.rotate(self.current_angle)
            self.state_image = ImageTk.PhotoImage(rotated)
            self.current_angle += self.rotation_angle

        self.canvas.create_image(
            self.winfo_width() // 2,
            self.winfo_height() // 2,
            image=self.state_image
        )
        self.timer_id = self.after(self.animation_wait_time, self.animate)

    def stop_animation(self) -> None:
        if self.current_animation_id is None:
            return

        if self.timer_id:
            self.after_cancel(self.timer_id)

        self.master.after_cancel(self.current_animation_id)
        self.canvas.delete(tk.ALL)
        self.stop_event.set()

        self.current_animation_id = None