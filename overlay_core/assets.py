from pathlib import Path
import os


root = Path(os.getcwd())
assets = root / Path('assets')
news_ico = assets / Path('news.ico')
loading_image = assets / Path('loading.png')