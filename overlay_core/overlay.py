import multiprocessing
import threading
from itertools import cycle
from typing import Any

from PIL import Image

from .language import *
from .spiders import (
    get_news_list,
    spiders_list,
    TITLE_KEY,
    TEXT_KEY,
    TIME_KEY,
    ORIGINAL_URL_KEY,
)
from .tray import Tray
from .assets import loading_image
from .buttons import (
    CloseButton,
    HoverButton,
    ExpandButton
)
from .wait_window import WaitWindow
from .components import (
    NewsItem,
    GrayFrame,
    ScrollableFrame
)
from .settings import SettingsWindow
from .constants import (
    ROOT_WIDTH,
    ROOT_DEFAULT_ALPHA,
    ROOT_HEIGHT,
    ROOT_DEFAULT_BG,
    FOREGROUND,
    SETTING_BUTTON_DEFAULT_FG,
    SETTING_BUTTON_DEFAULT_ICON,
    REFRESH_BUTTON_DEFAULT_FG,
    REFRESH_BUTTON_DEFAULT_ICON,
    REFRESH_BUTTON_ENTER_ICON,
    MESSAGE_TITLE_DEFAULT_FG,
    MESSAGE_TITLE_FOUND_FG,
    EVENT_KEY_RELEASE,
    EVENT_CONTROL_A,
    EVENT_CONTROL_BACKSPACE,
    BACKGROUND,
    SETTING_WINDOW_DEFAULT_BG,
    HIGHTLIGHTBACKGROUND
)


class LoadingAnimation(WaitWindow):
    animation_image = Image.open(loading_image).resize((60, 60))
    animation_wait_time = 50


def to_queue(provider, variable) -> None:
    variable.set(get_news_list(provider))


class Overlay:

    def __init__(self, tray: Tray):
        self.root = tk.Tk()
        self.tray = tray
        self.manager = multiprocessing.Manager()
        self.news_variable = self.manager.Value('news', [])
        self.news_items: List[NewsItem] = []
        self.language_state = LanguageState(additional_traces=[
            ('write', self.translate_news_context_menu)
        ])
        self.provider_variable = tk.StringVar()
        self.autorun_variable = tk.BooleanVar()
        self.providers = {
            class_obj.name: class_obj
            for class_obj in spiders_list
        }
        self.languages = list(languages.keys())
        self.find_matches: cycle[NewsItem] = cycle([])
        self.app_width = ROOT_WIDTH
        self.app_height = ROOT_HEIGHT

        self.provider_variable.set(list(self.providers)[0])
        self.language_state.language_variable.set(self.languages[0])

        self.root.configure(bg=ROOT_DEFAULT_BG)
        self.root.attributes('-alpha', ROOT_DEFAULT_ALPHA)

        self.screen_width = int(self.root.winfo_screenwidth())
        self.screen_height = int(self.root.winfo_screenheight())

        self.init_news()
        self.init_tool_panel()
        self.init_expand()

        self.expand_button_width = self._expand.winfo_reqwidth()
        self.expand_button_height = self._expand.winfo_height()

        self.hide_app()
        self.root.overrideredirect(True)
        self.root.lift()
        self.root.wm_attributes("-topmost", True)

        self.tray.item('Hide overlay')(self.hide_overlay)
        self.tray.item('Show overlay')(self.show_overlay)
        self.tray.item('Exit')(self.stop)
        self.tray.run()

        self.hide_overlay()

    def init_expand(self) -> None:
        self._expand = ExpandButton(master=self.root)
        app_expand_cycle = cycle([
            self.expand_app,
            self.hide_app
        ])

        @self._expand.on_click
        def expand() -> None:
            next(app_expand_cycle)()

    def init_tool_panel(self) -> None:
        self.tool_panel = GrayFrame(
            master=self.root
        )

        close_button = CloseButton(master=self.tool_panel)
        close_button.pack(side=tk.LEFT)

        settings_button = HoverButton(master=self.tool_panel)
        settings_button[FOREGROUND] = SETTING_BUTTON_DEFAULT_FG
        settings_button.string.set(SETTING_BUTTON_DEFAULT_ICON)
        settings_button.pack(side=tk.LEFT)

        refresh_button = HoverButton(master=self.tool_panel)
        refresh_button[FOREGROUND] = REFRESH_BUTTON_DEFAULT_FG
        refresh_button.string.set(REFRESH_BUTTON_DEFAULT_ICON)
        refresh_button.pack(side=tk.LEFT)

        search_text = tk.StringVar()
        search_field = tk.Entry(master=self.tool_panel, textvariable=search_text)
        search_field.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        def move_to_item(news_item: NewsItem) -> None:
            last_y = self.news_items[-1].winfo_y()
            current_y = news_item.winfo_y()
            current_height = news_item.winfo_height()
            difference = (current_y - current_height * 1.5) / last_y

            self.news_frame.canvas.yview_moveto(difference)

        def find_article(event: tk.Event) -> None:
            """
            Help function for find articles
            :param event:
            :return:
            """
            search = search_text.get().lower()

            if not search:
                for news_item in self.news_items:
                    news_item.title.config(fg=MESSAGE_TITLE_DEFAULT_FG)
                return

            if event.char == '\r' and self.find_matches:
                move_to_item(next(self.find_matches))
                return

            recent_matches = []
            for news_item in self.news_items:
                news_item.title.config(fg=MESSAGE_TITLE_DEFAULT_FG)
                if search in news_item.title_text.lower():
                    recent_matches.append(news_item)
                    news_item.title.config(fg=MESSAGE_TITLE_FOUND_FG)

            self.find_matches = cycle(recent_matches)

            if recent_matches:
                move_to_item(recent_matches[0])

        search_field.bind(EVENT_KEY_RELEASE, find_article)
        search_field.bind(EVENT_CONTROL_BACKSPACE, lambda _: search_text.set(''))
        search_field.bind(EVENT_CONTROL_A, lambda _: search_field.select_from(0))

        @refresh_button.on_enter
        def set_another() -> None:
            refresh_button.string.set(REFRESH_BUTTON_ENTER_ICON)

        @refresh_button.on_leave
        def set_default() -> None:
            refresh_button.string.set(REFRESH_BUTTON_DEFAULT_ICON)

        @refresh_button.on_click
        def refresh() -> None:
            self.update_news()

        @close_button.on_click
        def close(*_) -> None:
            self.stop()

        @settings_button.on_click
        def settings() -> None:
            SettingsWindow(
                master=self.root,
                provider_variable=self.provider_variable,
                language_state=self.language_state,
                autorun_variable=self.autorun_variable,
                provider_choices=list(self.providers),
                language_choices=self.languages
            )

    def init_news(self) -> None:
        self.news_frame = ScrollableFrame(
            master=self.root
        )
        animation = LoadingAnimation(
            master=self.root,
            canvas={
                BACKGROUND: SETTING_WINDOW_DEFAULT_BG,
                HIGHTLIGHTBACKGROUND: SETTING_WINDOW_DEFAULT_BG
            },
            bg=SETTING_WINDOW_DEFAULT_BG,
        )
        self.delete_all_news()
        provider = self.get_provider()

        def complete_wrapper() -> None:
            variable = self.news_variable
            process = multiprocessing.Process(
                target=to_queue,
                args=(provider, variable)
            )
            process.start()
            process.join()
            animation.stop_animation()
            animation.destroy()

            news = self.news_variable.get()
            self.append_news_list(new_news=news)
            self.show_overlay()

        threading.Thread(target=animation.start_animation).start()
        threading.Thread(target=complete_wrapper).start()

    def translate_news_context_menu(self, *_) -> None:
        for item in self.news_items:
            item.context_menu.destroy()
            item.context_menu = item.get_context_menu(
                label_copy_title=self.language_state[COPY_TITLE].get(),
                label_copy_text=self.language_state[COPY_MESSAGE].get(),
                label_copy_article=self.language_state[COPY_ARTICLE].get()
            )

    def get_provider(self) -> Any:
        return self.providers[self.provider_variable.get()]

    def delete_all_news(self) -> None:
        for news_item in self.news_items:
            news_item.destroy()
        self.news_items.clear()

    def update_news(self) -> None:
        self.delete_all_news()

        bad_item = NewsItem(
            master=self.news_frame.scrollable_frame,
            event_title=self.language_state[BAD_LABEL].get(),
            event_text='',
            event_time='',
            event_original_url='',
            language_state=self.language_state
        )
        bad_item.pack(expand=True, fill=tk.BOTH, padx=18, pady=10)

        provider = self.get_provider()
        new_news = get_news_list(provider)

        bad_item.destroy()
        self.append_news_list(new_news=new_news)

    def append_news_list(self, new_news: List[dict]) -> None:
        for news in new_news:

            title = news[TITLE_KEY]
            text = news[TEXT_KEY]
            time = news[TIME_KEY]
            original = news[ORIGINAL_URL_KEY]

            if (
                (title is None or not title.replace('\n', ''))
                or (text is None or not text.replace('\n', ''))
            ):
                continue

            item = NewsItem(
                master=self.news_frame.scrollable_frame,
                event_title=title,
                event_text=text,
                event_time=time,
                event_original_url=original,
                language_state=self.language_state
            )
            item.pack(expand=True, fill=tk.BOTH, padx=18, pady=10)
            self.news_items.append(item)

    def expand_app(self) -> None:
        self.tool_panel.pack(fill=tk.X)
        self.news_frame.pack(fill=tk.BOTH, expand=True)
        self._expand.place(
            y=(self.app_height - self._expand.winfo_height()) // 2, relx=0, x=2
        )
        self.root.geometry(
            f"{self.app_width}x{self.app_height}"
            f"+{self.screen_width - self.app_width}+{(self.screen_height - self.app_height) // 2}"
        )

    def hide_app(self) -> None:
        self.tool_panel.forget()
        self.news_frame.forget()
        self._expand.pack()

        size_string = f"{self._expand.winfo_width()}x{self._expand.winfo_height()}"

        self.root.geometry(
            f"{size_string if size_string != '1x1' else ''}"
            f"+{self.screen_width - self.expand_button_width}"
            f"+{(self.screen_height - self.expand_button_height) // 2}"
        )

    def run(self) -> None:
        self.root.mainloop()

    def stop(self, *_) -> None:
        self.show_overlay()
        self.root.quit()
        self.tray.stop()

    def hide_overlay(self, *_) -> None:
        self.root.withdraw()

    def show_overlay(self, *_) -> None:
        self.root.deiconify()
