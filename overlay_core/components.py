import tkinter as tk
from itertools import cycle
import webbrowser

import pyperclip

from .constants import (
    BACKGROUND,
    RELIEF,
    BORDERWIDTH,
    GRAY_FRAME_DEFAULT_BORDERWIDTH,
    GRAY_FRAME_DEFAULT_BG,
    GRAY_FRAME_DEFAULT_RELIEF,
    WIDTH,
    MESSAGE_TITLE_WIDTH,
    MESSAGE_TEXT_WIDTH,
    MESSAGE_FRAME_WIDTH,
    MESSAGE_TITLE_DEFAULT_BG,
    MESSAGE_TITLE_DEFAULT_FG,
    MESSAGE_TITLE_DEFAULT_FONT,
    MESSAGE_TEXT_DEFAULT_FG,
    MESSAGE_TEXT_DEFAULT_BG,
    MESSAGE_TEXT_DEFAULT_FONT,
    SCROLLABLE_FRAME_DEFAULT_BG,
    EVENT_LEFT_BUTTON,
    EVENT_RIGHT_BUTTON,
    EVENT_MOUSE_WHEEL,
    EVENT_CONFIGURE,
    EVENT_LEAVE,
    EVENT_ENTER, CURSOR_HAND_ONE, CURSOR_TOP_SIDE, CURSOR_BOTTOM_SIDE,
)
from .language import (
    LanguageState,
    COPY_TITLE,
    COPY_MESSAGE,
    COPY_ARTICLE
)


class GrayFrame(tk.Frame):

    def __init__(
            self,
            master: tk.Misc,
            **kwargs
    ):
        default_kwargs = {
            BACKGROUND: GRAY_FRAME_DEFAULT_BG,
            RELIEF: GRAY_FRAME_DEFAULT_RELIEF,
            BORDERWIDTH: GRAY_FRAME_DEFAULT_BORDERWIDTH
        }
        default_kwargs.update(kwargs)
        super(GrayFrame, self).__init__(
            master=master,
            **default_kwargs
        )


class NewsItem(GrayFrame):

    def __init__(
            self,
            master: tk.Misc,
            event_title: str,
            event_text: str,
            event_time: str,
            event_original_url: str,
            language_state: LanguageState,
            event_image: str = None,
            **kwargs
    ):
        default_kwargs = {
            WIDTH: MESSAGE_FRAME_WIDTH
        }
        default_kwargs.update(kwargs)
        super(NewsItem, self).__init__(
            master=master, **default_kwargs
        )
        self.image = event_image
        self.language_state = language_state

        self.title = tk.Message(
            master=self,
            font=MESSAGE_TITLE_DEFAULT_FONT,
            bg=MESSAGE_TITLE_DEFAULT_BG,
            fg=MESSAGE_TITLE_DEFAULT_FG,
            text=event_title,
            width=MESSAGE_TITLE_WIDTH,
            cursor=CURSOR_BOTTOM_SIDE
        )
        self.title.pack()

        self.time = tk.Message(
            master=self,
            font=MESSAGE_TITLE_DEFAULT_FONT,
            bg=MESSAGE_TITLE_DEFAULT_BG,
            fg=MESSAGE_TITLE_DEFAULT_FG,
            text=event_time,
            width=MESSAGE_TITLE_WIDTH,
            cursor=CURSOR_BOTTOM_SIDE
        )
        self.time.pack()

        self.url = tk.Message(
            master=self,
            font=MESSAGE_TITLE_DEFAULT_FONT,
            bg=MESSAGE_TITLE_DEFAULT_BG,
            fg=MESSAGE_TITLE_DEFAULT_FG,
            text=event_original_url,
            width=MESSAGE_TITLE_WIDTH,
            cursor=CURSOR_HAND_ONE,
        )
        self.url.pack()
        self.url.bind(
            EVENT_LEFT_BUTTON,
            lambda e: webbrowser.open(event_original_url)
        )

        self.message_text = event_text
        self.title_text = event_title
        self.message_string = tk.StringVar()
        self.message = tk.Message(
            master=self,
            font=MESSAGE_TEXT_DEFAULT_FONT,
            bg=MESSAGE_TEXT_DEFAULT_BG,
            fg=MESSAGE_TEXT_DEFAULT_FG,
            textvariable=self.message_string,
            width=MESSAGE_TEXT_WIDTH,
            cursor=CURSOR_BOTTOM_SIDE
        )
        self.message.pack()

        expand_list = cycle([
            self.maximize_message,
            self.minimize_message
        ])
        cursors_list = cycle([
            CURSOR_TOP_SIDE,
            CURSOR_BOTTOM_SIDE,
        ])
        expandable = [
            self,
            self.title,
            self.time,
            self.message
        ]

        def expand_(_) -> None:
            cursor = next(cursors_list)
            for wdgt in expandable:
                wdgt.configure(cursor=cursor)
            next(expand_list)()

        self.minimize_message()

        for widget in expandable:
            widget.bind(EVENT_LEFT_BUTTON, expand_)
            widget.bind(EVENT_RIGHT_BUTTON, self.show_context_menu)

        self.init_context_menu()

    def minimize_message(self) -> None:
        splited_text = self.message_text.split(' ', maxsplit=15)[:15]
        self.message_string.set(
            ' '.join(splited_text) + ' ....'
        )

    def maximize_message(self) -> None:
        self.message_string.set(self.message_text)

    def get_context_menu(
            self,
            label_copy_title: str,
            label_copy_text: str,
            label_copy_article: str
    ) -> tk.Menu:
        context_menu = tk.Menu(self, tearoff=0)

        context_menu.add_command(
            label=label_copy_title,
            command=lambda: pyperclip.copy(self.title_text)
        )
        context_menu.add_command(
            label=label_copy_text,
            command=lambda: pyperclip.copy(self.message_text)
        )
        context_menu.add_command(
            label=label_copy_article,
            command=lambda: pyperclip.copy(
                self.title_text + '\n\n' + self.message_text
            )
        )
        return context_menu

    def init_context_menu(self) -> None:
        self.context_menu = self.get_context_menu(
            label_copy_title=self.language_state[COPY_TITLE].get(),
            label_copy_text=self.language_state[COPY_MESSAGE].get(),
            label_copy_article=self.language_state[COPY_ARTICLE].get()
        )

    def show_context_menu(self, event: tk.Event) -> None:
        self.context_menu.tk_popup(event.x_root, event.y_root)


class ScrollableFrame(GrayFrame):
    def __init__(self, master: tk.Misc, *args, **kwargs):
        kwargs.update({BACKGROUND: SCROLLABLE_FRAME_DEFAULT_BG})
        super(ScrollableFrame, self).__init__(
            master=master, *args, **kwargs
        )

        self.canvas = canvas = tk.Canvas(
            master=self,
            bg=SCROLLABLE_FRAME_DEFAULT_BG,
            highlightbackground=SCROLLABLE_FRAME_DEFAULT_BG
        )
        scrollbar = tk.Scrollbar(self, orient=tk.VERTICAL, command=canvas.yview)
        self.scrollable_frame = frame = GrayFrame(canvas)

        def _configure_interior(_):
            size = (frame.winfo_reqwidth(), frame.winfo_reqheight())
            canvas.config(scrollregion="0 0 %s %s" % size)
            if frame.winfo_reqwidth() != canvas.winfo_width():
                canvas.config(width=frame.winfo_reqwidth())
        frame.bind(EVENT_CONFIGURE, _configure_interior)

        interior_id = canvas.create_window((0, 0), window=self.scrollable_frame, anchor=tk.NW)

        def _configure_canvas(_):
            if frame.winfo_reqwidth() != canvas.winfo_width():
                canvas.itemconfigure(interior_id, width=canvas.winfo_width())

        canvas.bind(EVENT_CONFIGURE, _configure_canvas)

        canvas.configure(yscrollcommand=scrollbar.set)
        canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

        frame.bind(EVENT_ENTER, self.on_enter)
        frame.bind(EVENT_LEAVE, self.on_leave)

    def on_enter(self, _):
        self.canvas.bind_all(EVENT_MOUSE_WHEEL, self.on_mousewheel)

    def on_leave(self, _):
        self.canvas.unbind_all(EVENT_MOUSE_WHEEL)

    def on_mousewheel(self, event: tk.Event):
        self.canvas.yview_scroll(int(-1 * (event.delta / 120)), tk.UNITS)
