import tkinter as tk
from typing import Any, Dict, List

from .constants import (
    BORDERWIDTH,
    BACKGROUND,
    SETTING_CHECK_DEFAULT_FG,
    SETTING_SELECT_DEFAULT_BG,
    SETTING_WINDOW_DEFAULT_BG,
    SETTING_CHECK_DEFAULT_BG,
    SETTING_DEFAULT_BORDERWIDTH,
    SETTING_LABEL_DEFAULT_FG,
    SETTING_CHECK_DEFAULT_BORDERWIDTH,
    SETTING_DEFAULT_BG,
    SETTING_LABEL_DEFAULT_BG,
    SETTING_WINDOW_DEFAULT_HEIGHT,
    SETTING_WINDOW_DEFAULT_WIDTH,
)
from .components import GrayFrame
from .language import (
    LanguageState,
    NEWS_PROVIDER,
    LANGUAGE_SELECT,
    AUTORUN_CHECK
)
from .buttons import CloseButton


class Setting(GrayFrame):

    def __init__(
            self,
            master: tk.Misc,
            setting_name: tk.StringVar,
            **kwargs
    ):
        default_kwargs = {
            BORDERWIDTH: SETTING_DEFAULT_BORDERWIDTH,
            BACKGROUND: SETTING_DEFAULT_BG
        }
        default_kwargs.update(kwargs)
        super(Setting, self).__init__(master=master, **default_kwargs)
        tk.Label(
            master=self,
            textvariable=setting_name,
            bg=SETTING_LABEL_DEFAULT_BG,
            fg=SETTING_LABEL_DEFAULT_FG
        ).pack(fill=tk.X, padx=5, pady=(10, 4))


class SelectSetting(Setting):

    def __init__(
            self,
            master: tk.Misc,
            values: Dict[str, Any],
            variable: tk.Variable,
            setting_name: tk.StringVar,
            **kwargs
    ):
        try:
            command = kwargs.pop(tk.COMMAND)
        except KeyError:
            command = None

        super(SelectSetting, self).__init__(
            master=master,
            setting_name=setting_name,
            **kwargs
        )

        select = tk.OptionMenu(
            self,
            variable,
            *values,
            command=command,
        )
        select.config(
            bg=SETTING_SELECT_DEFAULT_BG,
            fg=SETTING_LABEL_DEFAULT_FG,
            activebackground=SETTING_SELECT_DEFAULT_BG,
            activeforeground=SETTING_LABEL_DEFAULT_FG
        )
        select.pack(fill=tk.X, padx=5, pady=(0, 10))


class CheckSetting(GrayFrame):

    def __init__(
            self,
            master: tk.Misc,
            setting_name: tk.StringVar,
            bool_variable: tk.BooleanVar,
            **kwargs
    ):
        default_kwargs = {
            BACKGROUND: SETTING_CHECK_DEFAULT_BG,
            BORDERWIDTH: SETTING_CHECK_DEFAULT_BORDERWIDTH
        }
        default_kwargs.update(kwargs)
        super(CheckSetting, self).__init__(master=master, **default_kwargs)

        tk.Checkbutton(
            master=self,
            textvariable=setting_name,
            variable=bool_variable,
            bg=SETTING_CHECK_DEFAULT_BG,
            activebackground=SETTING_CHECK_DEFAULT_BG,
            activeforeground=SETTING_CHECK_DEFAULT_FG,
            fg=SETTING_CHECK_DEFAULT_FG,
            onvalue=True,
            offvalue=False,
        ).pack(fill=tk.X, padx=5, pady=(0, 10))


class SettingsWindow(tk.Toplevel):

    def __init__(
            self,
            master: tk.Misc,
            provider_variable: tk.StringVar,
            autorun_variable: tk.BooleanVar,
            provider_choices: List[str],
            language_choices: List[str],
            language_state: LanguageState,
            **kwargs
    ):
        default_kwargs = {BACKGROUND: SETTING_WINDOW_DEFAULT_BG}
        default_kwargs.update(kwargs)
        super(SettingsWindow, self).__init__(master=master, **default_kwargs)

        self.provider_variable = provider_variable
        self.language_state = language_state
        self.autorun_variable = autorun_variable

        self.language_choices = language_choices
        self.provider_choices = provider_choices

        self.grab_set()
        self.overrideredirect(True)
        self.lift()

        width = SETTING_WINDOW_DEFAULT_WIDTH
        height = SETTING_WINDOW_DEFAULT_HEIGHT
        screen_height = self.winfo_screenheight()
        screen_width = self.winfo_screenwidth()

        self.geometry(
            f"{width}x{height}"
            f"+{(screen_width - width) // 2}"
            f"+{(screen_height - height) // 2}"
        )

        self.init_top_menu()
        self.init_settings()

    def init_top_menu(self) -> None:
        menu_frame = GrayFrame(
            master=self,
            borderwidth=0
        )
        menu_frame.pack(side=tk.TOP, fill=tk.X)

        close_button = CloseButton(master=menu_frame)
        close_button.pack(side=tk.LEFT)

        @close_button.on_click
        def close() -> None:
            self.grab_release()
            self.destroy()

    def init_settings(self) -> None:

        SelectSetting(
            master=self,
            setting_name=self.language_state[NEWS_PROVIDER],
            values=self.provider_choices,
            variable=self.provider_variable
        ).pack(fill=tk.X, pady=5, padx=5)

        SelectSetting(
            master=self,
            setting_name=self.language_state[LANGUAGE_SELECT],
            variable=self.language_state.language_variable,
            values=self.language_choices
        ).pack(fill=tk.X, pady=5, padx=5)

        CheckSetting(
            master=self,
            setting_name=self.language_state[AUTORUN_CHECK],
            bool_variable=self.autorun_variable
        ).pack(fill=tk.X, pady=5, padx=5)
