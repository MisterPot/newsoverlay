import winreg


KEY = "Software\Microsoft\Windows\CurrentVersion\Run"
VALUE_NAME = 'NewsOverlay'


def UseKey(mode: int) -> winreg.HKEYType:
    return winreg.OpenKeyEx(winreg.HKEY_CURRENT_USER, KEY, 0, mode)


def is_overlay_in_autorun() -> bool:
    with UseKey(winreg.KEY_READ) as key:
        try:
            winreg.QueryValueEx(key, VALUE_NAME)
            return True
        except FileNotFoundError:
            return False


def add_to_autorun(filepath: str) -> bool:

    if is_overlay_in_autorun():
        return False

    with UseKey(winreg.KEY_WRITE) as key:
        winreg.SetValueEx(key, VALUE_NAME, 0, winreg.REG_SZ, filepath)
    return True


def delete_from_autorun(value_name: str) -> bool:
    if not is_overlay_in_autorun():
        return False

    with UseKey(winreg.KEY_WRITE) as key:
        winreg.DeleteValue(key, value_name)
    return True

