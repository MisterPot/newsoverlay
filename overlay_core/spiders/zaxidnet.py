import scrapy
from scrapy.http.response.html import HtmlResponse


class ZaxidNetSpider(scrapy.Spider):

    name = 'zaxidnet'
    start_urls = ['https://zaxid.net/novini_ukrayini_tag50959/']

    def parse(self, response: HtmlResponse, **kwargs):
        news_elements = response.xpath(
            '//li[contains(@class, "default-news-list")]/a[starts-with(@href, "https://zaxid.net")]'
        )
        for element in news_elements:
            url = element.xpath('.//@href').get()
            time = element.xpath('.//div[@class="time"]/text()').get()
            yield scrapy.Request(url, callback=self.parse_page, meta={'time': time})

    def parse_page(self, response: HtmlResponse):
        article_xpath = '//div[@id="newsSummary"]'
        text_xpath = './/p[not(contains(@style, "display:none"))]' \
                     '[not(contains(@style, "display: none"))]' \
                     '[not(contains(@style, "overflow: hidden"))]' \
                     ' | .//ul/li'
        string_list = response.xpath(article_xpath)[0]\
            .xpath(text_xpath)\
            .xpath('string(.)')\
            .getall()
        yield {
            'title': response.xpath('//h1[@id="newsName"]/text()').get(),
            'text': '\n\n'.join(string_list),
            'time': response.meta['time'],
            'original': response.url
        }
