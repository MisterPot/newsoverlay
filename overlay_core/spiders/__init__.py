from pathlib import Path
from typing import List, Dict

from scrapy import signals
from scrapy.signalmanager import dispatcher
from scrapy.crawler import CrawlerProcess
import scrapy.utils.misc
import scrapy.core.scraper

from .zaxidnet import ZaxidNetSpider
from .bbc_ukraine import BBCSpider
from .tsn import TSNSpider
from .censornet import CensorNetSpider


def warn_on_generator_with_return_value_stub(*_):
    pass


scrapy.utils.misc.warn_on_generator_with_return_value = warn_on_generator_with_return_value_stub
scrapy.core.scraper.warn_on_generator_with_return_value = warn_on_generator_with_return_value_stub


TITLE_KEY = 'title'
TEXT_KEY = 'text'
TIME_KEY = 'time'
ORIGINAL_URL_KEY = 'original'
root = Path(__file__).parent


spiders_list = [
    BBCSpider,
    CensorNetSpider,
    TSNSpider,
    ZaxidNetSpider
]


def get_news_list(news_spider: scrapy.Spider) -> List[Dict[str, str]]:
    results = []
    process = CrawlerProcess()
    process.crawl(news_spider)

    def crawler_results(signal, sender, item, response, spider):
        results.append(item)

    dispatcher.connect(crawler_results, signal=signals.item_scraped)

    try:
        process.start()
        return results
    except:
        import sys
        del sys.modules['twisted.internet.reactor']
        from twisted.internet import reactor
        from twisted.internet import default
        default.install()
        return get_news_list(news_spider)
