import scrapy
from scrapy.http.response.html import HtmlResponse


class TSNSpider(scrapy.Spider):

    name = 'tsn'
    start_urls = ['https://tsn.ua/']

    def parse(self, response: HtmlResponse, **kwargs):
        news_elements = response.xpath('//section[@class="l-sidebar"]//a[@class="c-card__link"]')

        for element in news_elements:
            url = element.xpath('.//@href').get()
            time = element.xpath('.//parent::h3/following-sibling::footer//time/text()').get()
            yield scrapy.Request(url=url, callback=self.parse_page, meta={'time': time})

    def parse_page(self, response: HtmlResponse):
        title = response.xpath('string(//h1[@class="c-card__title"])').get()
        strings = response.xpath('//div[@data-content]/p')\
            .xpath('string(.)').getall()[:-1]
        yield {
            'title': title,
            'text': '\n\n'.join(strings),
            'time': response.meta['time'],
            'original': response.url
        }
