import scrapy
from scrapy.http.response.html import HtmlResponse


class CensorNetSpider(scrapy.Spider):
    name = 'censornet'
    start_urls = ['https://censor.net/']

    def start_requests(self):
        for url in self.start_urls:
            yield scrapy.Request(url=url, callback=self.parse, headers={
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/110.0"
            })

    def parse(self, response: HtmlResponse, **kwargs):
        news_elements_xpath = '//div[contains(@class, "news-feed-tab active")]' \
                              '//span[contains(@class, "__title")]/parent::a'
        news_elements = response.xpath(news_elements_xpath)
        for element in news_elements:
            url = element.xpath('.//@href').get()
            time = element.xpath('.//following-sibling::span/time/text()').get()
            yield scrapy.Request(url=url, callback=self.parse_page, headers={
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/110.0"
            }, meta={'time': time})

    def parse_page(self, response: HtmlResponse):
        title = response.xpath('//h1[@class="news-title"]/text()').get()
        strings = response.xpath('//div[contains(@class,"news-text")]/p[not(@class="related-news")]') \
            .xpath('string(.)').getall()
        yield {
            'title': title,
            'text': '\n\n'.join(strings),
            'time': response.meta['time'],
            'original': response.url,
        }
