import scrapy
from scrapy.http.response.html import HtmlResponse
from urllib.parse import urljoin


class BBCSpider(scrapy.Spider):

    name = 'bbc_ukraine'
    start_urls = ['https://www.bbc.com/ukrainian']

    def parse(self, response: HtmlResponse, **kwargs):
        main_url = self.start_urls[0]
        news_elements = response.xpath(
            '//a[contains(@class, "focusIndicatorDisplayInlineBlock")][not(contains(@href, "youtube"))]'
        )
        for element in news_elements:
            url = element.xpath('.//@href').get()
            time = element.xpath('.//parent::h3/following-sibling::time/text()').get()
            yield scrapy.Request(url=urljoin(main_url, url), callback=self.parse_page, meta={'time': time})

    def parse_page(self, response: HtmlResponse):
        title = response.xpath('//div/h1/text()').get() or response.xpath('//div/strong/text()').get()
        string_list = response.xpath('//main/div[@dir]/p | //main/div[@dir]/ul/li')\
            .xpath('string(.)').getall()

        yield {
            'title': title,
            'text': '\n\n'.join(string_list),
            'time': response.meta['time'],
            'original': response.url
        }
