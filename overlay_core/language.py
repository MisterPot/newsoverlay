import tkinter as tk
from typing import Dict, Callable, List, Tuple, Literal


BAD_LABEL = 'bad_label'
NEWS_PROVIDER = 'news_provider'
LANGUAGE_SELECT = 'language_select'
AUTORUN_CHECK = 'autorun_check'
COPY_TITLE = 'copy_title'
COPY_MESSAGE = "copy_message"
COPY_ARTICLE = 'copy_article'


UA = 'ua'
EN = 'en'


languages = {
    EN: {
        BAD_LABEL: 'If you see this message, something comes wrong.'
                     'Try to change news provider to another.'
                     'Or maybe you can\'t connect to global network',
        NEWS_PROVIDER: 'News provider',
        LANGUAGE_SELECT: 'Select language',
        AUTORUN_CHECK: 'Run, when Windows starts',
        COPY_TITLE: 'Copy title',
        COPY_MESSAGE: 'Copy message',
        COPY_ARTICLE: 'Copy full article'
    },
    UA: {
        BAD_LABEL: 'Якщо ви бачите це повідомлення, значить щось пішло не так.'
                     'Спробуйте змінити провайдера новин на іншого.'
                     'Або, можливо, ви не підключені до глобальної мережі',
        NEWS_PROVIDER: 'Провайдер новин',
        LANGUAGE_SELECT: 'Вибрати мову',
        AUTORUN_CHECK: 'Запускати під час запуску Віндовс',
        COPY_TITLE: 'Скопіювати заголовок',
        COPY_MESSAGE: 'Скопіювати текст статті',
        COPY_ARTICLE: 'Скопіювати статтю повністю'
    },
}


Trace = Tuple[Literal['read', 'write', 'unset', 'array'], Callable]


class LanguageState:

    def __init__(self, additional_traces: List[Trace] = None):
        self.language_variable = tk.StringVar(value=EN)
        self.translations: Dict[str, tk.StringVar] = {}

        self._init_keys()

        if additional_traces is None:
            additional_traces = []

        for trace_mode, trace_callback in additional_traces:
            self.language_variable.trace_add(
                mode=trace_mode,
                callback=trace_callback
            )

        self.language_variable.trace_add(
            mode='write',
            callback=self.update_translation
        )

    def update_translation(self, *_) -> None:
        current_language = self.language_variable.get()
        for key in self.translations.keys():
            self.translations[key].set(
                value=languages[current_language][key]
            )

    def _init_keys(self) -> None:
        self.translations.update({
            key: self.changeable_string(key)
            for key in languages[EN].keys()
        })

    def changeable_string(self, key: str) -> tk.StringVar:
        default_value = languages[self.language_variable.get()][key]
        return tk.StringVar(value=default_value)

    def __getitem__(self, item) -> tk.StringVar:
        return self.translations[item]
