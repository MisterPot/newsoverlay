from typing import Callable

from pystray import Menu, MenuItem, Icon
from PIL import Image


TrayCallback = Callable[[Icon, MenuItem], None]


class Tray:

    def __init__(self, icon_image_path: str, icon_name: str):
        self.items = []
        self.icon = None
        self.icon_name = icon_name
        self.image = Image.open(icon_image_path)

    def init_icon(self) -> None:
        self.icon = Icon(self.icon_name, icon=self.image, menu=Menu(*self.items))

    def item(self, text: str) -> Callable[[TrayCallback], None]:
        def wrapper(function: TrayCallback) -> None:
            self.items.append(MenuItem(text=text, action=function))
            return function
        return wrapper

    def stop(self) -> None:
        self.icon.stop()

    def run(self) -> None:
        if self.icon is None:
            self.init_icon()
        self.icon.run_detached()
